$(function() {
  if (localStorage.getItem("token") == undefined) {
    window.location.assign("login.html?#");
  }

  // For live server
  let APIurl = 'http://bsnltv-loadbalancer-1826859284.ap-south-1.elb.amazonaws.com:5555/';
  // For testing server
  // let APIurl = "http://13.232.214.252:8000/";

  // For live server
  // let tokenID = 'b094473f431f0eefe841bd6d3f743b3135587f88';
  // For testing server
  // let tokenID = '90aaf925eb0126f1ae8d0a4af6ac7842ac6874e0';

  const loader = $("#loader");
  const loadPercentage = document.getElementById("load-percentage");

  $(document).on("click", "#logout-button", function() {
    localStorage.removeItem("token");
    window.location.assign("login.html?#");
  });

  function fetchGenres(done) {
    $.ajax({
      url: `${APIurl}api/v1/listgenre/`,
      type: "GET",
      headers: {
        Authorization: `token ${localStorage.getItem("token")}`
      },
      success: function(data) {
        // console.log(data);
        done(data);
      },
      error: function() {
        alert("Error: Genres could not be loaded");
      }
    });
  }
  let selectGenre = $("#select-genre");
  fetchGenres(function(leads) {
    selectGenre.empty();
    for (lead of leads) {
      selectGenre.append(updateGenreOption(lead));
    }
  });
  function updateGenreOption(lead) {
    return $(`
    <option value="${lead.name}" class="select-genre-option" id="${lead.id}">${lead.name}</option>
    `);
  }

  function fetchLanguages(done) {
    $.ajax({
      url: `${APIurl}api/v1/listlanguage/`,
      type: "GET",
      headers: {
        Authorization: `token ${localStorage.getItem("token")}`
      },
      success: function(data) {
        // console.log(data);
        done(data);
      },
      error: function() {
        alert("Error: Languages could not be loaded");
      }
    });
  }
  let selectLanguage = $("#select-language");
  fetchLanguages(function(leads) {
    selectLanguage.empty();
    for (lead of leads) {
      selectLanguage.append(updateLangOption(lead));
    }
  });
  function updateLangOption(lead) {
    return $(`
      <option value="${lead.name}" class="select-language-option" id="${lead.id}">${lead.name}</option>
    `);
  }

  // let uploadVideoValidationStatus = false;
  function uploadVideoValidation() {
    if (document.getElementById("video-to-upload").files[0] == undefined || document.getElementById("video-to-upload").files[0].type != "video/mp4") {
      document.getElementById("video-to-upload-validation-msg").innerHTML =
        "Please choose a valid mp4 video";
      // uploadVideoValidationStatus = false;
    } else {
      document.getElementById("video-to-upload-validation-msg").innerHTML = "";
      // uploadVideoValidationStatus = true;
    }
    if (document.getElementById("thumbnail-to-upload").files[0] == undefined || document.getElementById("thumbnail-to-upload").files[0].type != "image/jpeg") {
      document.getElementById("thumbnail-to-upload-validation-msg").innerHTML =
        "Please choose a valid jpg image";
      // uploadVideoValidationStatus = false;
    } else {
      document.getElementById("thumbnail-to-upload-validation-msg").innerHTML = "";
      // uploadVideoValidationStatus = true;
    }
    if (document.getElementById("upload-title").value == ""){
      document.getElementById("upload-title-validation-msg").innerHTML =
        "This field is required";
      // uploadVideoValidationStatus = false;
    }
    else {
      document.getElementById("upload-title-validation-msg").innerHTML = "";
      // uploadVideoValidationStatus = true;
    }
    if (document.getElementById("upload-description").value == ""){
      document.getElementById("upload-description-validation-msg").innerHTML =
        "This field is required";
      // uploadVideoValidationStatus = false;
    }
    else {
      document.getElementById("upload-description-validation-msg").innerHTML = "";
      // uploadVideoValidationStatus = true;
    }
  }

  const uploadVideoButton = $("#upload-video-btn");
  uploadVideoButton.click(function() {
    uploadVideoValidation();
    if (
      document.getElementById("video-to-upload-validation-msg").innerHTML == "" &&
      document.getElementById("thumbnail-to-upload-validation-msg").innerHTML == "" &&
      document.getElementById("upload-title-validation-msg").innerHTML == "" &&
      document.getElementById("upload-description-validation-msg").innerHTML == ""
    ) {
      loader.css("display", "block");
      var fd = new FormData();
      fd.append("data", document.getElementById("video-to-upload").files[0]);
      fd.append(
        "thumbnail",
        document.getElementById("thumbnail-to-upload").files[0]
      );
      fd.append("title", document.getElementById("upload-title").value);
      var fdTitle = document.getElementById("upload-title").value;
      fd.append(
        "description",
        document.getElementById("upload-description").value
      );
      fd.append("category", document.getElementById("select-category").value);
      let genreSelectedIndex = document.getElementById("select-genre")
        .selectedIndex;
      fd.append(
        "genre",
        document
          .getElementById("select-genre")
          .querySelectorAll(".select-genre-option")[genreSelectedIndex].id
      );
      let languageSelectedIndex = document.getElementById("select-language")
        .selectedIndex;
      // console.log(languageSelectedIndex);
      fd.append(
        "language",
        document
          .getElementById("select-language")
          .querySelectorAll(".select-language-option")[languageSelectedIndex].id
      );
      $.ajax({
        url: `${APIurl}api/v1/uploadContent/`,
        type: "POST",
        processData: false,
        contentType: false,
        headers: {
          Authorization: `token ${localStorage.getItem("token")}`
        },
        data: fd,
        xhr: function() {
          var jqXHR = null;
          if (window.ActiveXObject) {
            jqXHR = new window.ActiveXObject("Microsoft.XMLHTTP");
          } else {
            jqXHR = new window.XMLHttpRequest();
          }

          //Upload progress
          jqXHR.upload.addEventListener(
            "progress",
            function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = Math.round(
                  (evt.loaded * 100) / evt.total
                );
                //Do something with upload progress
                // console.log("Uploaded percent", percentComplete);
                loadPercentage.innerHTML = `<span>${percentComplete}%</span>`;
              }
            },
            false
          );

          // //Download progress
          // jqXHR.addEventListener( "progress", function ( evt )
          // {
          //     if ( evt.lengthComputable )
          //     {
          //         var percentComplete = Math.round( (evt.loaded * 100) / evt.total );
          //         //Do something with download progress
          //         console.log( 'Downloaded percent', percentComplete );
          //     }
          // }, false );

          return jqXHR;
        },
        success: function(data) {
          // console.log(data);
          loader.css("display", "none");
          loadPercentage.innerHTML = "";
          document.getElementById(
            "success-modal-body"
          ).innerHTML = `Video "${fdTitle}" successfully published!`;
          $("#successModal").modal();
        },
        error: function() {
          loader.css("display", "none");
          loadPercentage.innerHTML = "";
          alert("An error was encountered.");
        }
      });
      document.getElementById("upload-form").reset();
      resetThumbnailView();
      resetVideoView();
    }
  });
});
