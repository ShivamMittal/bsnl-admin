$(function() {
  if (localStorage.getItem("token") != undefined) {
    window.location.assign("../../index.html");
  }

  // For live server
  let APIurl = "http://bsnltv-loadbalancer-1826859284.ap-south-1.elb.amazonaws.com:5555/";
  // For testing server
  // let APIurl = "http://13.232.214.252:8000/";

  function loginValidation() {
    if (document.getElementById("login-phone-field").value == "") {
      document.getElementById("login-phone-field-validation-msg").innerHTML =
        "This field is required";
    } else {
      document.getElementById("login-phone-field-validation-msg").innerHTML =
        "";
    }
    if (document.getElementById("login-password-field").value == "") {
      document.getElementById("login-password-field-validation-msg").innerHTML =
        "This field is required";
    } else {
      document.getElementById("login-password-field-validation-msg").innerHTML =
        "";
    }
  }

  const logInButton = $("#log-in-button");
  logInButton.click(function() {
    loginValidation();
    if (
      document.getElementById("login-phone-field-validation-msg")
        .innerHTML == "" &&
      document.getElementById("login-password-field-validation-msg")
        .innerHTML == ""
    ) {
      $.ajax({
        url: `${APIurl}accounts/v1/login/`,
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          phone: document.getElementById("login-phone-field").value,
          password: document.getElementById("login-password-field").value
        }),
        dataType: "json",
        processData: false,
        success: function(data) {
          // console.log(data);
          localStorage.removeItem("token");
          localStorage.setItem("token", data.token);
          window.location.assign("../../index.html");
        }
      });
    }
  });
});
