$(function() {
  if (localStorage.getItem("token") == undefined) {
    window.location.assign("login.html?#");
  }

  // For live server
  let APIurl = 'http://bsnltv-loadbalancer-1826859284.ap-south-1.elb.amazonaws.com:5555/';
  // For testing server
  // let APIurl = "http://13.232.214.252:8000/";

  // For live server
  // let tokenID = 'b094473f431f0eefe841bd6d3f743b3135587f88';
  // For testing server
  // let tokenID = '90aaf925eb0126f1ae8d0a4af6ac7842ac6874e0';

  const loader = $("#loader");
  const loadPercentage = document.getElementById("load-percentage");

  let paginationIndex = 1;

  $(document).on("click", "#logout-button", function() {
    localStorage.removeItem("token");
    window.location.assign("login.html?#");
  });

  let incrementPaginationBtn = $("#increment-pagination-btn");
  incrementPaginationBtn.click(function() {
    paginationIndex += 1;
    getVideos();
    document.getElementById("pagination-index").innerHTML = paginationIndex;
  });

  let decrementPaginationBtn = $("#decrement-pagination-btn");
  decrementPaginationBtn.click(function() {
    if (paginationIndex >= 2) {
      paginationIndex -= 1;
      getVideos();
      document.getElementById("pagination-index").innerHTML = paginationIndex;
    }
  });

  if (sessionStorage.contentListAPIData == undefined) {
    sessionStorage.setItem(
      "contentListAPIData",
      JSON.stringify({
        filter_opt: {}
      })
    );
  }

  // let contentListAPIData = JSON.stringify({
  //   filter_opt: {}
  // });

  getVideos();
  document.getElementById("pagination-index").innerHTML = paginationIndex;

  function getVideos() {
    loader.css("display", "block");

    document.getElementById("videos-tbody").innerHTML = "";
    $.ajax({
      url: `${APIurl}api/v1/content/list/?page=${paginationIndex}`,
      type: "POST",
      contentType: "application/json",
      headers: {
        Authorization: `token ${localStorage.getItem("token")}`
      },
      data: sessionStorage.getItem("contentListAPIData"),
      dataType: "json",
      processData: false,
      success: function(data) {
        // try {
        // console.log(data);
        var videosBody = document.getElementById("videos-tbody");
        for (i = 0; i < data.length; i++) {
          var videosRow = document.createElement("tr");
          for (j = 0; j < 9; j++) {
            var videosData = document.createElement("td");
            videosRow.appendChild(videosData);
          }
          videosBody.appendChild(videosRow);
          var currentVideo = data[i];
          // console.log(currentVideo);
          if (paginationIndex == 1) {
            // console.log(paginationIndex);
            videosRow.querySelectorAll("td")[0].innerHTML = i + 1;
          } else {
            // console.log(paginationIndex);
            videosRow.querySelectorAll("td")[0].innerHTML =
              (paginationIndex - 1) * 10 + i + 1;
          }
          var videoThumbnail = document.createElement("img");
          videoThumbnail.src = currentVideo.thumbnail;
          videosRow.querySelectorAll("td")[1].appendChild(videoThumbnail);
          videosRow.querySelectorAll("td")[2].innerHTML = currentVideo.title;
          videosRow.querySelectorAll("td")[3].innerHTML = currentVideo.id;
          videosRow.querySelectorAll("td")[4].innerHTML = currentVideo.category;
          videosRow.querySelectorAll("td")[5].innerHTML = currentVideo.genre;
          videosRow.querySelectorAll("td")[6].innerHTML = currentVideo.language;
          videosRow.querySelectorAll(
            "td"
          )[7].style.backgroundColor = currentVideo.active
            ? "#42ff42"
            : "orange";
          videosRow.querySelectorAll("td")[7].innerHTML = currentVideo.active
            ? "Active"
            : "Suspended";
          var editButton = `<button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#editContentModal" title="${
            currentVideo.title
          }" description="${currentVideo.description}" thumbnail="${
            currentVideo.thumbnail
          }" data="${currentVideo.data}" language="${
            currentVideo.language
          }" genre="${currentVideo.genre}" category="${
            currentVideo.category
          }" id="${currentVideo.id}" status="${
            currentVideo.active ? "active" : "suspended"
          }" onclick="handleEdit(this)">Edit</button>`;
          videosRow.querySelectorAll("td")[8].innerHTML = editButton;
        }
        loader.css("display", "none");
        //       }
        // catch {
        //   loader.css("display", "none");
        //   alert('An error was encountered.');
        // }
      },
      error: function() {
        loader.css("display", "none");
        alert("An error was encountered.");
      }
    });
  }

  function fetchGenres(done) {
    $.ajax({
      url: `${APIurl}api/v1/listgenre/`,
      type: "GET",
      headers: {
        Authorization: `token ${localStorage.getItem("token")}`
      },
      dataType: "json",
      processData: false,
      success: function(data) {
        // console.log(data);
        done(data);
      },
      error: function() {
        alert("Error: Genres could not be loaded");
      }
    });
  }
  let editGenre = $("#edit-genre");
  fetchGenres(function(leads) {
    editGenre.empty();
    for (lead of leads) {
      editGenre.append(updateGenreOption(lead));
    }
  });
  function updateGenreOption(lead) {
    return $(`
    <option value="${lead.name}" class="edit-genre-option" id="${lead.id}">${lead.name}</option>
    `);
  }

  function fetchLanguages(done) {
    $.ajax({
      url: `${APIurl}api/v1/listlanguage/`,
      type: "GET",
      headers: {
        Authorization: `token ${localStorage.getItem("token")}`
      },
      success: function(data) {
        // console.log(data);
        done(data);
      },
      error: function() {
        alert("Error: Languages could not be loaded");
      }
    });
  }
  let languageFilter = $("#language-filter");
  let editLanguage = $("#edit-language");
  fetchLanguages(function(leads) {
    languageFilter.empty();
    languageFilter.append(
      `<option class="dropdown-item language-option" id="all-language-filter">All languages</option>`
    );
    editLanguage.empty();
    for (lead of leads) {
      languageFilter.append(createNewLead(lead));
      editLanguage.append(updateLangOption(lead));
    }
  });
  function createNewLead(lead) {
    return $(`
      <option class="dropdown-item language-option mainCLass" data-id="${lead.name}" value="${lead.name}">${lead.name}</option>
    `);
  }
  function updateLangOption(lead) {
    return $(`
    <option value="${lead.name}" class="edit-language-option" id="${lead.id}">${lead.name}</option>
    `);
  }

  $(document).on("click", "#all-language-filter", function() {
    sessionStorage.contentListAPIData = JSON.stringify({
      filter_opt: {}
    });
    paginationIndex = 1;
    getVideos();
    document.getElementById("pagination-index").innerHTML = paginationIndex;
    selectionIdentify();
  });

  $(document).on("click", ".mainCLass", function() {
    value = $(this).attr("data-id");
    // console.log("The name is ", value);
    sessionStorage.contentListAPIData = JSON.stringify({
      filter_opt: {
        alllanguagedata: value
      }
    });
    paginationIndex = 1;
    getVideos();
    document.getElementById("pagination-index").innerHTML = paginationIndex;
    selectionIdentify();
  });

  const videoSearchButton = $("#video-search-button");
  videoSearchButton.click(function() {
    if (document.getElementById("video-search").value !== "") {
      sessionStorage.contentListAPIData = JSON.stringify({
        search: document.getElementById("video-search").value,
        filter_opt: {}
      });
    } else {
      sessionStorage.contentListAPIData = JSON.stringify({
        filter_opt: {}
      });
    }
    paginationIndex = 1;
    getVideos();
    document.getElementById("pagination-index").innerHTML = paginationIndex;
    selectionIdentify();
  });

  function selectionIdentify() {
    let selectionIdentifier = JSON.parse(sessionStorage.contentListAPIData);
    if (selectionIdentifier.filter_opt.alllanguagedata) {
      document.getElementById("language-identifier").innerHTML =
        selectionIdentifier.filter_opt.alllanguagedata;
    } else {
      document.getElementById("language-identifier").innerHTML = "Language";
    }
    if (selectionIdentifier.search) {
      document.getElementById("video-search").value =
        selectionIdentifier.search;
    } else {
      document.getElementById("video-search").value = "";
    }
  }
  selectionIdentify();

  // let updateVideoValidationStatus = false;
  function updateVideoValidation() {
    if(document.getElementById("video-to-upload").files[0]){
      if(document.getElementById("video-to-upload").files[0].type != "video/mp4"){
        document.getElementById("video-to-upload-validation-msg").innerHTML =
        "not mp4";
        // updateVideoValidationStatus = false;
      }
      else {
        document.getElementById("video-to-upload-validation-msg").innerHTML = "";
        // updateVideoValidationStatus = true;
      }
    }
    else {
      document.getElementById("video-to-upload-validation-msg").innerHTML = "";
      // updateVideoValidationStatus = true;
    }
    if(document.getElementById("thumbnail-to-upload").files[0]){
      if(document.getElementById("thumbnail-to-upload").files[0].type != "image/jpeg"){
        document.getElementById("thumbnail-to-upload-validation-msg").innerHTML =
        "not jpg";
        // updateVideoValidationStatus = false;
      }
      else {
        document.getElementById("thumbnail-to-upload-validation-msg").innerHTML = "";
        // updateVideoValidationStatus = true;
      }
    }
    else {
      document.getElementById("thumbnail-to-upload-validation-msg").innerHTML = "";
      // updateVideoValidationStatus = true;
    }
    if (document.getElementById("edit-title").value == "") {
      document.getElementById("edit-title-validation-msg").innerHTML =
        "This field is required";
      // updateVideoValidationStatus = false;
    } else {
      document.getElementById("edit-title-validation-msg").innerHTML = "";
      // updateVideoValidationStatus = true;
    }
    if (document.getElementById("edit-description").value == "") {
      document.getElementById("edit-description-validation-msg").innerHTML =
        "This field is required";
      // updateVideoValidationStatus = false;
    } else {
      document.getElementById("edit-description-validation-msg").innerHTML = "";
      // updateVideoValidationStatus = true;
    }
  }

  const updateVideoButton = $("#update-video-btn");
  updateVideoButton.click(function(e) {
    updateVideoValidation();
    if (
      document.getElementById("video-to-upload-validation-msg").innerHTML == "" &&
      document.getElementById("thumbnail-to-upload-validation-msg").innerHTML == "" &&
      document.getElementById("edit-title-validation-msg").innerHTML == "" &&
      document.getElementById("edit-description-validation-msg").innerHTML == ""
      ) {
      loader.css("display", "block");
      let contentId = this.getAttribute("value");
      var fd = new FormData();
      if (document.getElementById("video-to-upload").files[0]) {
        fd.append("data", document.getElementById("video-to-upload").files[0]);
      }
      if (document.getElementById("thumbnail-to-upload").files[0]) {
        fd.append(
          "thumbnail",
          document.getElementById("thumbnail-to-upload").files[0]
        );
      }
      fd.append("title", document.getElementById("edit-title").value);
      var fdTitle = document.getElementById("edit-title").value;
      fd.append(
        "description",
        document.getElementById("edit-description").value
      );
      fd.append("category", document.getElementById("edit-category").value);
      let genreSelectedIndex = document.getElementById("edit-genre")
        .selectedIndex;
      fd.append(
        "genre",
        document
          .getElementById("edit-genre")
          .querySelectorAll(".edit-genre-option")[genreSelectedIndex].id
      );
      let languageSelectedIndex = document.getElementById("edit-language")
        .selectedIndex;
      // console.log(languageSelectedIndex);
      fd.append(
        "language",
        document
          .getElementById("edit-language")
          .querySelectorAll(".edit-language-option")[languageSelectedIndex].id
      );
      let chosenStatus =
        document.getElementById("edit-status").value == "true" ? true : false;
      fd.append("active", chosenStatus);
      $.ajax({
        url: `${APIurl}api/v1/content/update/${contentId}/`,
        type: "PUT",
        processData: false,
        contentType: false,
        headers: {
          Authorization: `token ${localStorage.getItem("token")}`
        },
        data: fd,
        xhr: function() {
          var jqXHR = null;
          if (window.ActiveXObject) {
            jqXHR = new window.ActiveXObject("Microsoft.XMLHTTP");
          } else {
            jqXHR = new window.XMLHttpRequest();
          }

          //Upload progress
          jqXHR.upload.addEventListener(
            "progress",
            function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = Math.round(
                  (evt.loaded * 100) / evt.total
                );
                //Do something with upload progress
                // console.log("Uploaded percent", percentComplete);
                loadPercentage.innerHTML = `<span>${percentComplete}%</span>`;
              }
            },
            false
          );

          // //Download progress
          // jqXHR.addEventListener( "progress", function ( evt )
          // {
          //     if ( evt.lengthComputable )
          //     {
          //         var percentComplete = Math.round( (evt.loaded * 100) / evt.total );
          //         //Do something with download progress
          //         console.log( 'Downloaded percent', percentComplete );
          //     }
          // }, false );

          return jqXHR;
        },
        success: function(data) {
          // console.log(data);
          $("#editContentModal").modal("hide");
          loader.css("display", "none");
          loadPercentage.innerHTML = "";
          document.getElementById(
            "success-modal-body"
          ).innerHTML = `Video "${fdTitle}" successfully updated!`;
          $("#successModal").modal();
          getVideos();
        },
        error: function() {
          $("#editContentModal").modal("hide");
          loader.css("display", "none");
          loadPercentage.innerHTML = "";
          alert("An error was encountered.");
          getVideos();
        }
      });
      document.getElementById("video-update-form").reset();
      resetThumbnailView();
      resetVideoView();
    }
  });
});
