$(function() {
  if (localStorage.getItem("token") == undefined) {
    window.location.assign("login.html?#");
  }

  // For live server
  let APIurl = 'http://bsnltv-loadbalancer-1826859284.ap-south-1.elb.amazonaws.com:5555/';
  // For testing server
  // let APIurl = "http://13.232.214.252:8000/";

  // For live server
  // let tokenID = 'b094473f431f0eefe841bd6d3f743b3135587f88';
  // For testing server
  // let tokenID = '90aaf925eb0126f1ae8d0a4af6ac7842ac6874e0';

  const loader = $("#loader");
  const loadPercentage = document.getElementById("load-percentage");

  let paginationIndex = 1;

  // let bannerLangFilter = "";

  if (sessionStorage.bannerLangFilter == undefined) {
    sessionStorage.setItem("bannerLangFilter", "");
  }

  $(document).on("click", "#logout-button", function() {
    localStorage.removeItem("token");
    window.location.assign("login.html?#");
  });

  let incrementPaginationBtn = $("#increment-pagination-btn");
  incrementPaginationBtn.click(function() {
    paginationIndex += 1;
    getBanners();
    document.getElementById("pagination-index").innerHTML = paginationIndex;
  });

  let decrementPaginationBtn = $("#decrement-pagination-btn");
  decrementPaginationBtn.click(function() {
    if (paginationIndex >= 2) {
      paginationIndex -= 1;
      getBanners();
      document.getElementById("pagination-index").innerHTML = paginationIndex;
    }
  });

  getBanners();
  document.getElementById("pagination-index").innerHTML = paginationIndex;

  function getBanners() {
    loader.css("display", "block");
    document.getElementById("banners-tbody").innerHTML = "";
    $.ajax({
      url: `${APIurl}api/v1/banners/?page=${paginationIndex}&language=${sessionStorage.bannerLangFilter}`,
      type: "GET",
      contentType: "application/json",
      headers: {
        Authorization: `token ${localStorage.getItem("token")}`
      },
      data: JSON.stringify({}),
      dataType: "json",
      processData: false,
      success: function(data) {
        // console.log(data.data);
        var bannersBody = document.getElementById("banners-tbody");
        for (i = 0; i < data.data.length; i++) {
          var bannersRow = document.createElement("tr");
          for (j = 0; j < 6; j++) {
            var bannersData = document.createElement("td");
            bannersRow.appendChild(bannersData);
          }
          bannersBody.appendChild(bannersRow);
          var currentBanner = data.data[i];
          // console.log(currentBanner);
          if (paginationIndex == 1) {
            // console.log(paginationIndex);
            bannersRow.querySelectorAll("td")[0].innerHTML = i + 1;
          } else {
            // console.log(paginationIndex);
            bannersRow.querySelectorAll("td")[0].innerHTML =
              (paginationIndex - 1) * 10 + i + 1;
          }
          var bannerPic = document.createElement("img");
          bannerPic.src = currentBanner.banner_pic;
          bannersRow.querySelectorAll("td")[1].appendChild(bannerPic);
          bannersRow.querySelectorAll("td")[2].innerHTML =
            currentBanner.content.title;

          bannersRow.querySelectorAll("td")[3].innerHTML = currentBanner.id;
          bannersRow.querySelectorAll("td")[4].innerHTML =
            currentBanner.banner.language;
          var editButton = `<button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#editBannerModal" onclick="handleEdit(this); hideDeleteConfirmation();" banner-id="${currentBanner.id}" banner-pic="${currentBanner.banner_pic}" content-id="${currentBanner.content.id}" content-lang="${currentBanner.content.language}">Edit</button>`;
          bannersRow.querySelectorAll("td")[5].innerHTML = editButton;
        }
        loader.css("display", "none");
      },
      error: function() {
        loader.css("display", "none");
        alert("An error was encountered.");
      }
    });
  }

  function fetchLanguages(done) {
    $.ajax({
      url: `${APIurl}api/v1/listlanguage/`,
      type: "GET",
      headers: {
        Authorization: `token ${localStorage.getItem("token")}`
      },
      success: function(data) {
        // console.log(data);
        done(data);
      },
      error: function() {
        alert("Error: Languages could not be loaded");
      }
    });
  }
  let languageFilter = $("#language-filter");
  let bannerLanguageUpload = $("#banner-language-upload");
  // let bannerLanguageUpdate = $('#banner-language-update');
  fetchLanguages(function(leads) {
    languageFilter.empty();
    languageFilter.append(
      `<option class="dropdown-item language-option" id="all-language-filter">All languages</option>`
    );
    bannerLanguageUpload.empty();
    bannerLanguageUpload.append(
      `<option value="Choose" class="banner-language-upload-option"">Choose</option>`
    );
    // bannerLanguageUpdate.empty();
    for (lead of leads) {
      languageFilter.append(createNewLead(lead));
      bannerLanguageUpload.append(addLangUploadOption(lead));
      // bannerLanguageUpdate.append(addLangUpdateOption(lead));
    }
  });
  function createNewLead(lead) {
    return $(`
      <option class="dropdown-item language-option mainCLass" data-id="${lead.name}" value="${lead.name}">${lead.name}</option>
    `);
  }
  function addLangUploadOption(lead) {
    return $(`
      <option value="${lead.name}" class="banner-language-upload-option" id="${lead.id}">${lead.name}</option>
    `);
  }
  // function addLangUpdateOption(lead) {
  //   return $(`
  //     <option value="${lead.name}" class="banner-language-update-option" id="${lead.id}">${lead.name}</option>
  //   `);
  // }

  $(document).on("click", "#all-language-filter", function() {
    sessionStorage.bannerLangFilter = "";
    paginationIndex = 1;
    getBanners();
    document.getElementById("pagination-index").innerHTML = paginationIndex;
    selectionIdentify();
  });

  $(document).on("click", ".mainCLass", function() {
    value = $(this).attr("data-id");
    // console.log("The name is ", value);
    sessionStorage.bannerLangFilter = value;
    paginationIndex = 1;
    getBanners();
    document.getElementById("pagination-index").innerHTML = paginationIndex;
    selectionIdentify();
  });

  function selectionIdentify() {
    let selectionIdentifier = sessionStorage.bannerLangFilter;
    if (selectionIdentifier != "") {
      document.getElementById(
        "language-identifier"
      ).innerHTML = selectionIdentifier;
    } else {
      document.getElementById("language-identifier").innerHTML = "Language";
    }
  }
  selectionIdentify();

  var contentForBannerUpload = $("#content-for-banner-upload");

  $(document).on("change", "#banner-language-upload", getVideosForBannerUpload);
  $(document).on("click", "#form-reset-btn-upload", getVideosForBannerUpload);
  
  function getVideosForBannerUpload(){
    loader.css("display", "block");
    document.getElementById("content-for-banner-upload").innerHTML = "";
    $.ajax({
      url: `${APIurl}api/v1/content/list/?page=0`,
      type: "POST",
      contentType: "application/json",
      headers: {
        Authorization: `token ${localStorage.getItem("token")}`
      },
      data: JSON.stringify({
        filter_opt: {
          alllanguagedata: document.getElementById("banner-language-upload")
            .value
        }
      }),
      dataType: "json",
      processData: false,
      success: function(data) {
        // console.log(data);
        for (i = 0; i < data.length; i++) {
          contentForBannerUpload.append(
            `<option class="content-for-banner-upload-opt" content-id="${data[i].id}">${data[i].title}</option>`
          );
        }
        loader.css("display", "none");
      },
      error: function() {
        loader.css("display", "none");
        alert("An error was encountered.");
      }
    });
  };

  // let uploadBannerValidationStatus = false;
  function uploadBannerValidation() {
    if (
      document.getElementById("banner-pic-to-upload").files[0] == undefined ||
      (document.getElementById("banner-pic-to-upload").files[0].type !=
        "image/jpeg" &&
        document.getElementById("banner-pic-to-upload").files[0].type !=
          "image/png")
    ) {
      document.getElementById("banner-pic-to-upload-validation-msg").innerHTML =
        "Please choose a valid jpg/png image";
      // uploadBannerValidationStatus = false;
    } else {
      document.getElementById("banner-pic-to-upload-validation-msg").innerHTML =
        "";
      // uploadBannerValidationStatus = true;
    }
    if (document.getElementById("banner-language-upload").value == "Choose") {
      document.getElementById(
        "banner-language-upload-validation-msg"
      ).innerHTML = "Choose Language";
      // uploadBannerValidationStatus = false;
    } else {
      document.getElementById(
        "banner-language-upload-validation-msg"
      ).innerHTML = "";
      // uploadBannerValidationStatus = true;
    }
    if (
      document.getElementById("content-for-banner-upload").value == "Choose" ||
      document.getElementById("content-for-banner-upload").value == ""
    ) {
      document.getElementById(
        "content-for-banner-upload-validation-msg"
      ).innerHTML = "Choose Content";
      // uploadBannerValidationStatus = false;
    } else {
      document.getElementById(
        "content-for-banner-upload-validation-msg"
      ).innerHTML = "";
      // uploadBannerValidationStatus = true;
    }
  }

  const uploadBannerButton = $("#upload-banner-btn");
  uploadBannerButton.click(function() {
    uploadBannerValidation();
    if (
      document.getElementById("banner-pic-to-upload-validation-msg")
        .innerHTML == "" &&
      document.getElementById("banner-language-upload-validation-msg")
        .innerHTML == "" &&
      document.getElementById("content-for-banner-upload-validation-msg")
        .innerHTML == ""
    ) {
      loader.css("display", "block");
      var fd = new FormData();
      fd.append(
        "banner_pic",
        document.getElementById("banner-pic-to-upload").files[0]
      );
      fd.append(
        "content",
        document
          .querySelectorAll(".content-for-banner-upload-opt")
          [contentForBannerUpload.prop("selectedIndex")].getAttribute(
            "content-id"
          )
      );
      let bannerLangSelectedIndex = document.getElementById(
        "banner-language-upload"
      ).selectedIndex;
      fd.append(
        "language",
        document
          .getElementById("banner-language-upload")
          .querySelectorAll(".banner-language-upload-option")[
          bannerLangSelectedIndex
        ].id
      );
      $.ajax({
        url: `${APIurl}api/v1/banner/create/`,
        type: "POST",
        processData: false,
        contentType: false,
        headers: {
          Authorization: `token ${localStorage.getItem("token")}`
        },
        data: fd,
        xhr: function() {
          var jqXHR = null;
          if (window.ActiveXObject) {
            jqXHR = new window.ActiveXObject("Microsoft.XMLHTTP");
          } else {
            jqXHR = new window.XMLHttpRequest();
          }

          //Upload progress
          jqXHR.upload.addEventListener(
            "progress",
            function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = Math.round(
                  (evt.loaded * 100) / evt.total
                );
                //Do something with upload progress
                // console.log("Uploaded percent", percentComplete);
                loadPercentage.innerHTML = `<span>${percentComplete}%</span>`;
              }
            },
            false
          );

          // //Download progress
          // jqXHR.addEventListener( "progress", function ( evt )
          // {
          //     if ( evt.lengthComputable )
          //     {
          //         var percentComplete = Math.round( (evt.loaded * 100) / evt.total );
          //         //Do something with download progress
          //         console.log( 'Downloaded percent', percentComplete );
          //     }
          // }, false );

          return jqXHR;
        },
        success: function(data) {
          // console.log(data);
          $("#addBannerModal").modal("hide");
          loader.css("display", "none");
          loadPercentage.innerHTML = "";
          document.getElementById("success-modal-body").innerHTML =
            "Banner successfully uploaded!";
          $("#successModal").modal();
          getBanners();
        },
        error: function() {
          $("#addBannerModal").modal("hide");
          loader.css("display", "none");
          loadPercentage.innerHTML = "";
          alert("An error was encountered.");
          getBanners();
        }
      });
      // document.getElementById("banner-upload-form").reset(); OR:
      $('#form-reset-btn-upload').click();
      resetbannerPicViewUpload();
    }
  });

  let bannerToDelete;
  const confirmDeleteButton = $("#confirm-delete-btn");
  confirmDeleteButton.click(function() {
    loader.css("display", "block");
    bannerToDelete = this.value;
    $.ajax({
      url: `${APIurl}api/v1/banners/delete/${bannerToDelete}/`,
      type: "DELETE",
      headers: {
        Authorization: `token ${localStorage.getItem("token")}`
      },
      success: function() {
        $("#editBannerModal").modal("hide");
        loader.css("display", "none");
        document.getElementById("success-modal-body").innerHTML =
          "Banner successfully deleted!";
        $("#successModal").modal();
        getBanners();
      },
      error: function() {
        $("#editBannerModal").modal("hide");
        loader.css("display", "none");
        alert("An error was encountered.");
        getBanners();
      }
    });
  });

  function updateBannerValidation() {
    if (
      document.getElementById("banner-pic-update").files[0] == undefined ||
      (document.getElementById("banner-pic-update").files[0].type !=
        "image/jpeg" &&
        document.getElementById("banner-pic-update").files[0].type !=
          "image/png")
    ) {
      document.getElementById("banner-pic-update-validation-msg").innerHTML =
        "Not a valid jpg/png image";
    } else {
      document.getElementById("banner-pic-update-validation-msg").innerHTML =
        "";
    }
  }

  let bannerToUpdate;
  const updateBannerButton = $("#update-banner-btn");
  updateBannerButton.click(function() {
    updateBannerValidation();
    if (
      document.getElementById("banner-pic-update-validation-msg")
        .innerHTML == ""
    ) {
      loader.css("display", "block");
      bannerToUpdate = this.value;
      var fd = new FormData();
      if (document.getElementById("banner-pic-update").files[0]) {
        fd.append(
          "banner_pic",
          document.getElementById("banner-pic-update").files[0]
        );
      }
      // fd.append("content", document.getElementById("videoID-update").value);
      // let bannerLangUpdateIndex = document.getElementById('banner-language-update').selectedIndex;
      // fd.append( 'language', document.getElementById('banner-language-update').querySelectorAll(".banner-language-update-option")[bannerLangUpdateIndex].id );
      $.ajax({
        url: `${APIurl}api/v1/banners/update/${bannerToUpdate}/`,
        type: "POST",
        processData: false,
        contentType: false,
        headers: {
          Authorization: `token ${localStorage.getItem("token")}`
        },
        data: fd,
        xhr: function() {
          var jqXHR = null;
          if (window.ActiveXObject) {
            jqXHR = new window.ActiveXObject("Microsoft.XMLHTTP");
          } else {
            jqXHR = new window.XMLHttpRequest();
          }

          //Upload progress
          jqXHR.upload.addEventListener(
            "progress",
            function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = Math.round(
                  (evt.loaded * 100) / evt.total
                );
                //Do something with upload progress
                // console.log("Uploaded percent", percentComplete);
                loadPercentage.innerHTML = `<span>${percentComplete}%</span>`;
              }
            },
            false
          );

          // //Download progress
          // jqXHR.addEventListener( "progress", function ( evt )
          // {
          //     if ( evt.lengthComputable )
          //     {
          //         var percentComplete = Math.round( (evt.loaded * 100) / evt.total );
          //         //Do something with download progress
          //         console.log( 'Downloaded percent', percentComplete );
          //     }
          // }, false );

          return jqXHR;
        },
        success: function(data) {
          // console.log(data);
          $("#editBannerModal").modal("hide");
          loader.css("display", "none");
          loadPercentage.innerHTML = "";
          document.getElementById("success-modal-body").innerHTML =
            "Banner successfully updated!";
          $("#successModal").modal();
          getBanners();
        },
        error: function() {
          $("#editBannerModal").modal("hide");
          loader.css("display", "none");
          loadPercentage.innerHTML = "";
          alert("An error was encountered.");
          getBanners();
        }
      });
      document.getElementById("banner-update-form").reset();
      resetbannerPicViewUpdate();
    }
  });
});
