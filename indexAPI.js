$(function() {
    if (localStorage.getItem("token") == undefined) {
        window.location.assign("src/views/login.html?#");
    }

    $(document).on("click", "#logout-button", function() {
        localStorage.removeItem("token");
        window.location.assign("src/views/login.html?#");
    });
})